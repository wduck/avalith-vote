/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.boundary;

import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.util.regex.Pattern;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import org.junit.jupiter.api.Test;

/**
 *
 * @author wduck
 */
@QuarkusTest
public class UsersResourceTest {
    
    String bearer;
    
    @Test
    public void testLoginEndpoint() {
        
        given()
          .pathParam("userName", "wduck@avalith.org")
          .pathParam("pwd", "12345")
          .when().get("/users/token/{userName}/{pwd}")
          .then()
             .statusCode(200)             
             .body("name",  is("wduck@avalith.org"));
             //.body("jwt",  matchesPattern(Pattern.compile("?")));        
        
    }
    

    
}
