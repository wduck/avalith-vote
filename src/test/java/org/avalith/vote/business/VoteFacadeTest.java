/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.business;

import com.google.inject.Inject;
import org.avalith.boundary.*;
import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.avalith.vote.VO.SummaryVoteReport;
import org.avalith.vote.VO.UserJWT;
import org.avalith.vote.VO.VoteRequest;
import org.avalith.vote.model.Vote;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 *
 * @author wduck
 */
// Integration Tests business
@QuarkusTest
public class VoteFacadeTest {

    private static final Logger LOGGER = Logger.getLogger(VoteFacadeTest.class.getName());
    
    
    
    @Inject 
    VoteFacade voteFacade;
    
    String username = "wduck@avalith.org";
    
    @Test
    public void testLogin() throws UnsupportedEncodingException, Exception {
        LOGGER.log(Level.INFO, "RUN testLogin");
        UserJWT user = voteFacade.login(username, "12345");
        Assertions.assertNotNull(user);
        Assertions.assertEquals(username, user.getName());
        Assertions.assertNotNull(user.getName());        
    }
    
    @Test
    public void testTotalUsersByRoleName(){
        LOGGER.log(Level.INFO, "RUN testTotalUsersByRoleName");
        Number number = voteFacade.totalUsersByRoleName("EMPLOYEE");        
        Assertions.assertNotEquals(0, number.intValue());          
    }
    
    @Test
    public void testRegisterVoteSuccess() {
        LOGGER.log(Level.INFO, "RUN testRegisterVote");
        VoteRequest voteRequest = new VoteRequest();
        voteRequest.setAreaTypeId(1L);
        voteRequest.setCandidateId(2L);
        voteRequest.setComment("VOTE by the best");
        voteRequest.setCurrentUser(username);
        
        Vote registerVote = voteFacade.registerVote(voteRequest);        
        LOGGER.log(Level.INFO, "RESULTS testRegisterVote {0}", registerVote.toString());
        Assertions.assertNotNull(registerVote);
          
    }
    
    @Test
    public void testRegisterExceptionVerifyUniqueVoteInMonth() {
        LOGGER.log(Level.INFO, "RUN testRegisterVote");
        VoteRequest voteRequest = new VoteRequest();
        voteRequest.setAreaTypeId(1L);
        voteRequest.setCandidateId(2L);
        voteRequest.setComment("VOTE Repeat");
        voteRequest.setCurrentUser(username);        
        Assertions.assertThrows(IllegalStateException.class,  () -> {
            voteFacade.registerVote(voteRequest);
        });         
    }
    
    @Test
    public void testRegisterExceptionVerifyPersonUserWithPersonVote() {
        LOGGER.log(Level.INFO, "RUN testRegisterVote");
        VoteRequest voteRequest = new VoteRequest();
        voteRequest.setAreaTypeId(1L);
        voteRequest.setCandidateId(1L);
        voteRequest.setComment("VOTE by the best");
        voteRequest.setCurrentUser(username);        
        Assertions.assertThrows(IllegalStateException.class,  () -> {
            voteFacade.registerVote(voteRequest);
        });         
    }
    
    @Test
    @DisplayName("CountVotesByAreaTypeAndDates")
    public void testCountVotesByAreaTypeAndDates(){
        LocalDate date = LocalDate.now();
        LocalDate startDate = date.withDayOfMonth(1);
        LocalDate endDate = date.withDayOfMonth(date.lengthOfMonth());
        List<SummaryVoteReport> results = voteFacade.countVotesByAreaTypeAndDates(1L, startDate, endDate);
        Assertions.assertNotNull(results);
    }
    
    
}
