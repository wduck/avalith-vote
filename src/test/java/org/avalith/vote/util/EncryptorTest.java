/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author wduck
 */
public class EncryptorTest {
    
    public EncryptorTest() {
    }
    
    @Test
    public void testToMD5() throws Exception {
        System.out.println("toMD5");
        String input = "GeeksForGeeks";
        String expResult = "e39b9c178b2c9be4e99b141d956c6ff6";
        String result = EncryptorUtil.toMD5(input);
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }
    
}
