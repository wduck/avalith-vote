/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.util;

import io.smallrye.jwt.build.Jwt;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author wduck
 */
public class TokenUtil {
    
    public static String generateJWT(String userName, ArrayList<String> rolesStr) throws Exception {
        
        long currentTimeInSecs = currentTimeInSecs();

        Map<String, Object> claimMap = new HashMap<>();
        claimMap.put("iss", "https://avalith.org");
        claimMap.put("sub", "jwt-rbac");
        claimMap.put("exp", currentTimeInSecs + 300);
        claimMap.put("iat", currentTimeInSecs);
        claimMap.put("auth_time", currentTimeInSecs);
        claimMap.put("jti", UUID.randomUUID().toString());
        claimMap.put("upn", "UPN");
        claimMap.put("groups", rolesStr);
        claimMap.put("raw_token", UUID.randomUUID().toString());
        claimMap.put("user_name", userName);

        return Jwt.claims(claimMap).jws().signatureKeyId("privateKey.pem").sign(readPrivateKey("privateKey.pem"));
    }
    
    
    public static PrivateKey readPrivateKey(final String pemResName) throws Exception {
        InputStream contentIS =  Thread.currentThread().getContextClassLoader().getResourceAsStream(pemResName);
        byte[] tmp = new byte[4096];
        int length = contentIS.read(tmp);
        return decodePrivateKey(new String(tmp, 0, length, "UTF-8"));
    }

    public static PrivateKey decodePrivateKey(final String pemEncoded) throws Exception {
        byte[] encodedBytes = toEncodedBytes(pemEncoded);

        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpec);
    }

    private static byte[] toEncodedBytes(final String pemEncoded) {
        final String normalizedPem = removeBeginEnd(pemEncoded);
        return Base64.getDecoder().decode(normalizedPem);
    }

    private static String removeBeginEnd(String pem) {
        pem = pem.replaceAll("-----BEGIN (.*)-----", "");
        pem = pem.replaceAll("-----END (.*)----", "");
        pem = pem.replaceAll("\r\n", "");
        pem = pem.replaceAll("\n", "");
        return pem.trim();
    }

    public static int currentTimeInSecs() {
        long currentTimeMS = System.currentTimeMillis();
        return (int) (currentTimeMS / 1000);
    }

}
