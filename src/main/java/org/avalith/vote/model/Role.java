/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.model;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wduck
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = {"name"}
))
@TableGenerator(
        name = "RoleGenerator",
        //schema = "vote",
        table = "Generator",
        pkColumnName = "name",
        valueColumnName = "value",
        pkColumnValue = "Role",
        initialValue = 10,
        allocationSize = 1)

public class Role implements Serializable{

    private static final Logger LOGGER = Logger.getLogger(Role.class.getName());    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "RoleGenerator")
    Long id;
    
    @NotNull
    String name;
    
    @NotNull
    String description;
    
    @ManyToMany(mappedBy = "roles")
    Set<User> users;

    public Role() {
        users = new LinkedHashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name.trim().toUpperCase();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Role other = (Role) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Role.class.getName());
        sb.append("{");
        sb.append("id:").append(id);
        sb.append(", name:").append(name);
        sb.append(", description:").append(description);
        sb.append('}');
        return sb.toString();
    }
    
    

    
    
    
}
