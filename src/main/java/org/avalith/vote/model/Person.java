/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.model;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wduck
 */
@Entity
@TableGenerator(
        name = "PersonGenerator",
        //schema = "vote",
        table = "Generator",
        pkColumnName = "name",
        valueColumnName = "value",
        pkColumnValue = "Person",
        initialValue = 0,
        allocationSize = 1)

public class Person implements Serializable {

    private static final Logger LOGGER = Logger.getLogger(Person.class.getName());

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "PersonGenerator")
    Long id;

    @NotNull
    String lastName;

    @NotNull
    String firstName;

    @ManyToMany
    @JoinTable(name = "person_vote",
            joinColumns = @JoinColumn(name = "person_id"),
            inverseJoinColumns = @JoinColumn(name = "vote_id"))
    Set<Vote> votes;

    public Person() {
        votes = new  LinkedHashSet();
    }

    public String name() {
        return firstName + " " + lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName.trim().toUpperCase();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName.trim().toUpperCase();
    }

}
