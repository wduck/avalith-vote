/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wduck
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = {"id", "name"}
))
@TableGenerator(
        name = "AreaTypeGenerator",
        //schema = "vote",
        table = "Generator",
        pkColumnName = "name",
        valueColumnName = "value",
        pkColumnValue = "AreaType",
        initialValue = 0,
        allocationSize = 1)

public class AreaType implements Serializable{

    private static final Logger LOGGER = Logger.getLogger(AreaType.class.getName());    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "AreaTypeGenerator")
    Long id;
    
    @NotNull
    String name;
    
    @NotNull
    String description;

    public AreaType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AreaType other = (AreaType) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(AreaType.class.getName());
        sb.append("{");
        sb.append("id:").append(id);
        sb.append(", name:").append(name);
        sb.append(", description:").append(description);
        sb.append('}');
        return sb.toString();
    }
    
    

    
    
    
}
