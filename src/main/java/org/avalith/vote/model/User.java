/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.model;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wduck
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = {"name"}
))

@TableGenerator(
        name = "UserGenerator",
        //schema = "vote",
        table = "Generator",
        pkColumnName = "name",
        valueColumnName = "value",
        pkColumnValue = "User",
        initialValue = 10,
        allocationSize = 1)

@NamedQueries(value = {    
    @NamedQuery(name = User.findByName, query = "select o From User o where o.name = :name "),
    @NamedQuery(name = User.totalByRoleName, query = "select COUNT(o.id) From User o LEFT JOIN o.roles r where r.name = :roleName ")
})

@NamedEntityGraphs(value = {
    @NamedEntityGraph(name = User.attributeGraphs,
            attributeNodes = {
                @NamedAttributeNode(value = "person"),
                @NamedAttributeNode(value = "roles")    
            })
})


public class User implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    @JsonbTransient
    private static final Logger LOGGER = Logger.getLogger(User.class.getName());    
    @JsonbTransient
    static final String PREFIX = "avalith.vote.User.";
    @JsonbTransient
    public static final String findByName = PREFIX + "findByName";
    @JsonbTransient
    public static final String totalByRoleName = PREFIX + "totalByRoleName";
    @JsonbTransient
    public static final String attributeGraphs = PREFIX + "attributeGraphs";
    

    @Id
    @GeneratedValue(generator = "UserGenerator")
    Long id;
    
    @Email
    @NotNull
    String name;
    
    @NotNull
    String password;
    
    @ManyToMany
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    Set<Role> roles;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    Person person;

    public User() {
        roles = new LinkedHashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {                
        this.name = name.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(@NotNull String password) {        
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(User.class.getName());
        sb.append("{");
        sb.append("id:").append(id);
        sb.append(", name:").append(name);
        sb.append(", password:").append(password);
        sb.append(", roles: [").append(roles).append("]");
        sb.append(", person: {").append(person).append("}");
        sb.append('}');
        return sb.toString();
    }

    
    
    

    
    
    
}
