/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Logger;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wduck
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(
        columnNames = {"date_", "areaType_id", "candidate_id"}
))
@TableGenerator(
        name = "VoteGenerator",
        //schema = "vote",
        table = "Generator",
        pkColumnName = "name",
        valueColumnName = "value",
        pkColumnValue = "Vote",
        initialValue = 0,
        allocationSize = 1)

@NamedQueries(value = {    
    @NamedQuery(name = Vote.findByAreaTypeAndCandidateAndDates, query = "SELECT v FROM Vote v WHERE v.areaType.id = :areaTypeId "
            + "AND v.candidate.id = :candidateId AND (v.date BETWEEN :startDate AND :endDate) ORDER BY v.date"),        
    @NamedQuery(name = Vote.countByAreaTypeAndDates, query = "SELECT NEW org.avalith.vote.VO.SummaryVoteReport(v.candidate, v.areaType, COUNT(v.id) as votes)"
            + "FROM Vote v WHERE v.areaType.id = :areaTypeId "
            + "AND (v.date BETWEEN :startDate AND :endDate) ORDER BY votes"),   
    @NamedQuery(name = Vote.findPersonsByDates, query = "SELECT NEW org.avalith.vote.VO.SummaryVoteReport(v.candidate, COUNT(v.id) as votes) "
            + "FROM Vote v WHERE "
            + "v.date BETWEEN :startDate AND :endDate ORDER BY votes")        
        
})

public class Vote implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @JsonbTransient
    private static final Logger LOGGER = Logger.getLogger(Vote.class.getName());
    
    @JsonbTransient
    static final String PREFIX = "avalith.vote.Vote.";
    @JsonbTransient
    public static final String findByAreaTypeAndCandidateAndDates = PREFIX + "findByAreaTypeAndCandidateAndDates";
    @JsonbTransient
    public static final String countByAreaTypeAndDates = PREFIX + "findPersonsByAreaTypeAndDates";
    @JsonbTransient
    public static final String findPersonsByDates = PREFIX + "findPersonsByDates";
    
    
    @Id
    @GeneratedValue(generator = "VoteGenerator")
    Long id;
    
    @NotNull
    @Column(name = "date_")
    LocalDate date;
    
    String comment;    
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    AreaType areaType;
    
    @ManyToMany (mappedBy = "votes")
    Set<Person> persons;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    Person candidate; 

    public Vote() {
        date = LocalDate.now();
        persons = new LinkedHashSet<>();
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    public Person getCandidate() {
        return candidate;
    }

    public void setCandidate(Person candidate) {
        this.candidate = candidate;
    }
    
    public void add(@NotNull Person person){
        getPersons().add(person);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.date);
        hash = 97 * hash + Objects.hashCode(this.areaType);
        hash = 97 * hash + Objects.hashCode(this.candidate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vote other = (Vote) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        if (!Objects.equals(this.areaType, other.areaType)) {
            return false;
        }
        if (!Objects.equals(this.candidate, other.candidate)) {
            return false;
        }
        return true;
    }
    
    
}
