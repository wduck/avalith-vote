/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.VO;

import io.quarkus.security.User;
import org.avalith.vote.model.AreaType;
import org.avalith.vote.model.Person;

/**
 *
 * @author wduck
 */
public class SummaryVoteReport implements java.io.Serializable{
    
    Person candidate;
    AreaType areaType;
       
    Number total;

    public SummaryVoteReport() {
    }

    public SummaryVoteReport(Person candidate, AreaType areaType, Number total) {
        this.candidate = candidate;
        this.areaType = areaType;
        this.total = total;
    }
    
    public SummaryVoteReport(Person candidate, Number total) {
        this.candidate = candidate;        
        this.total = total;
    }

    
    
    public Person getCandidate() {
        return candidate;
    }

    public void setCandidate(Person person) {
        this.candidate = person;
    }

    public AreaType getAreaType() {
        return areaType;
    }

    public void setAreaType(AreaType areaType) {
        this.areaType = areaType;
    }

    public Number getTotal() {
        return total;
    }

    public void setTotal(Number total) {
        this.total = total;
    }

    
    
    
}
