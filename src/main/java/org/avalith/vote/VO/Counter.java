/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.VO;

import io.quarkus.security.User;

/**
 *
 * @author wduck
 */
public class Counter implements java.io.Serializable{
    
    Number total;

    public Counter() {
    }

    public Counter(Number total) {
        this.total = total;
    }
    
    public Number getTotal() {
        return total;
    }

    public void setTotal(Number total) {
        this.total = total;
    }
    
    
}
