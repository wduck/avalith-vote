/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.VO;

import java.io.Serializable;
import java.time.LocalDate;
import javax.validation.constraints.NotNull;

/**
 *
 * @author wduck
 */
public class VoteRequest implements Serializable{
    
    @NotNull    
    Long areaTypeId;
    @NotNull
    Long candidateId;
    
    LocalDate date;
    
    String comment;
    
    String currentUser;

    public VoteRequest() {
        date = LocalDate.now();
    }

    public Long getAreaTypeId() {
        return areaTypeId;
    }

    public void setAreaTypeId(Long areaTypeId) {
        this.areaTypeId = areaTypeId;
    }

    public Long getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(Long candidateId) {
        this.candidateId = candidateId;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
    
    
    
    
    
}
