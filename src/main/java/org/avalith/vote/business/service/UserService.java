/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.business.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import org.avalith.vote.model.User;

/**
 *
 * @author wduck
 */
@ApplicationScoped
public class UserService {

    private static final Logger LOGGER = Logger.getLogger(UserService.class.getName());
    
    @Inject
    CrudService crudService;
    
    public User findByName(@NotNull final String name){ 
        LOGGER.log(Level.INFO, "Param Found: {0}", name);
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("name", name);
        LOGGER.log(Level.INFO, "Params Map: {0}", params);
        return (User)crudService.findSingleWithNamedQuery(User.findByName, params);
        
    }
    
    public Number totalUsersByRoleName(@NotNull final String roleName){
        LOGGER.log(Level.INFO, "Param Found: {0}", roleName);
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("roleName", roleName);
        try {
            Number number = (Number)crudService.findSingleWithNamedQuery(User.totalByRoleName, params);
            return number;
        } catch(EntityNotFoundException nex){
            return new Integer(0);
        }
    }
    
    public void checkPassword(@NotNull User user, final String password){
        if (!Objects.equals(user.getPassword(), password)){
            throw new SecurityException( "Password incorrect!");
        }
    }
    
    
    
}
