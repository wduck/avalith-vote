/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.business.service;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import org.avalith.vote.VO.SummaryVoteReport;
import org.avalith.vote.model.Vote;

/**
 *
 * @author wduck
 */
@ApplicationScoped
public class VoteService {

    private static final Logger LOGGER = Logger.getLogger(VoteService.class.getName());
    
    @Inject
    CrudService crudService;
    
    public List<Vote> findByAreaTypeAndCandidateAndDates(@NotNull final Long areaTypeId, 
            @NotNull final Long candidateId, LocalDate startDate, LocalDate endDate){ 
        
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("areaTypeId", areaTypeId);
        params.put("candidateId", candidateId);
        params.put("startDate", startDate != null ? startDate : LocalDate.now().minusDays(30));
        params.put("endDate", endDate != null ? endDate : LocalDate.now());
        
        LOGGER.log(Level.INFO, "Params Map: {0}", params);
        return crudService.findWithNamedQuery(Vote.findByAreaTypeAndCandidateAndDates, params);
        
    }
    
    @Transactional(Transactional.TxType.REQUIRED)
    public Vote save(Vote entity){
        if (entity.getId() == null) {
            return crudService.create(entity);
        } else {
            return crudService.update(entity);
        }
    }
    
    public List<SummaryVoteReport> countByAreaTypeAndDates(Long areaTypeId, LocalDate startDate, LocalDate endDate){
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("areaTypeId", areaTypeId);        
        params.put("startDate", startDate != null ? startDate : LocalDate.now().minusDays(30));
        params.put("endDate", endDate != null ? endDate : LocalDate.now());
        List<SummaryVoteReport> results = crudService.findWithNamedQuery(Vote.countByAreaTypeAndDates, params);
        return results;
    }
    
    public List<SummaryVoteReport> countByDates(LocalDate startDate, LocalDate endDate){
        Map<String, Object> params = new LinkedHashMap<>();
        params.put("startDate", startDate != null ? startDate : LocalDate.now().minusDays(30));
        params.put("endDate", endDate != null ? endDate : LocalDate.now());
        List<SummaryVoteReport> results = crudService.findWithNamedQuery(Vote.findPersonsByDates, params);
        return results;
    }
    
    
}
