/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.business.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.validation.constraints.NotNull;
import org.avalith.vote.model.Person;
import org.avalith.vote.model.User;

/**
 *
 * @author wduck
 */
@ApplicationScoped
public class PersonService {

    private static final Logger LOGGER = Logger.getLogger(PersonService.class.getName());
    
    @Inject
    CrudService crudService;
    
    public Person findById(@NotNull final Long id){ 
        LOGGER.log(Level.INFO, "Param Found: {0}", id);
        Person person = (Person)crudService.find(Person.class, id);
        return person;
    }
    
    
}
