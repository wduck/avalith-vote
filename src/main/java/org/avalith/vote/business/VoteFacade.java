/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.vote.business;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import static javax.transaction.Transactional.TxType.SUPPORTS;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.avalith.vote.VO.SummaryVoteReport;
import org.avalith.vote.business.service.UserService;
import org.avalith.vote.model.Role;
import org.avalith.vote.model.User;
import org.avalith.vote.VO.UserJWT;
import org.avalith.vote.VO.VoteRequest;
import org.avalith.vote.business.service.PersonService;
import org.avalith.vote.business.service.VoteService;
import org.avalith.vote.model.AreaType;
import org.avalith.vote.model.Person;
import org.avalith.vote.model.Vote;
import org.avalith.vote.util.EncryptorUtil;
import org.avalith.vote.util.TokenUtil;

/**
 *
 * @author wduck
 */
@ApplicationScoped
@Transactional(SUPPORTS)
public class VoteFacade {
    
    static final Logger LOGGER = Logger.getLogger(VoteFacade.class.getSimpleName());    

    @Inject
    UserService userService;
    
    @Inject
    PersonService personService;
    
    @Inject
    VoteService voteService;
    
    public UserJWT login (String username, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, Exception{
        User userFound = userService.findByName(username);
        userService.checkPassword(userFound, EncryptorUtil.toMD5(password));
        Set<Role> roles = userFound.getRoles();
        LOGGER.log(Level.INFO, "Roles Found: {0}", roles);
        ArrayList<String> rolesStr = new ArrayList<>();
        roles.forEach((role) -> {
            rolesStr.add(role.getName());
        });        
        LOGGER.log(Level.INFO, "Roles Found: {0}", rolesStr);
        
        UserJWT userJwt = new UserJWT();
        userJwt.setName(userFound.getName());
        
        userJwt.setJwt(TokenUtil.generateJWT(userFound.getName(), rolesStr));        
        return userJwt;
    }
    
    public Number totalUsersByRoleName(@NotNull final String roleName){
        return userService.totalUsersByRoleName(roleName);
    }
    
    public Vote registerVote(@Valid VoteRequest voteRequest){        
        User user = userService.findByName(voteRequest.getCurrentUser());
        verifyPersonUserWithPersonVote(user, voteRequest.getCandidateId());        
        Person candidate = personService.findById(voteRequest.getCandidateId());
        verifyVoteUnique(voteRequest);
        
        Vote vote = buildInstanceVote(user, candidate, voteRequest);
        vote = voteService.save(vote);
        return vote;     
    }
    
    public List<SummaryVoteReport> countVotesByAreaTypeAndDates(@NotNull Long areaTypeId, LocalDate startDate, LocalDate endDate){
        List<SummaryVoteReport> results = voteService.countByAreaTypeAndDates(areaTypeId, startDate, endDate);        
        return results;
    }
    
    public List<SummaryVoteReport> countVotesByDates(LocalDate startDate, LocalDate endDate){
        List<SummaryVoteReport> results = voteService.countByDates(startDate, endDate);        
        return results;
    }
    
    private Vote buildInstanceVote(User user, Person candidate, VoteRequest voteRequest){
        Vote vote = new Vote();
        vote.setCandidate(candidate);
        vote.setDate(voteRequest.getDate());
        vote.setComment(voteRequest.getComment());
        AreaType areaType = new AreaType();
        areaType.setId(voteRequest.getAreaTypeId());
        vote.setAreaType(areaType);
        vote.add(user.getPerson());
        return vote;
    }
    
    private void verifyPersonUserWithPersonVote(User user, Long candidateId){
        Long personId = user.getPerson().getId();
        if (Objects.equals(personId, candidateId)){
            throw new IllegalStateException("Illegal vote, can't suffer for you");
        }
        
    }
    
    private void verifyVoteUnique(VoteRequest voteRequest){
        LocalDate date = voteRequest.getDate();
        LocalDate startDate = date.withDayOfMonth(1);
        LocalDate endDate = date.withDayOfMonth(date.lengthOfMonth());
        
        List<Vote> votes = voteService.findByAreaTypeAndCandidateAndDates(voteRequest.getAreaTypeId(), 
                voteRequest.getCandidateId(), startDate, endDate);
        
        if (!votes.isEmpty()){
            throw new IllegalStateException("There is already a vote for this date: " + date.toString());
        }
    }
    
}
