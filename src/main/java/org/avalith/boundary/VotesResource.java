/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.boundary;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonString;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.avalith.vote.VO.Counter;
import org.avalith.vote.VO.SummaryVoteReport;
import org.avalith.vote.business.VoteFacade;
import org.avalith.vote.model.User;
import org.avalith.vote.VO.UserJWT;
import org.avalith.vote.VO.VoteRequest;
import org.avalith.vote.model.Vote;
import org.avalith.vote.util.DateTimeUtil;
import org.avalith.vote.util.TokenUtil;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;

/**
 *
 * @author wduck
 */
@RequestScoped
@Path("/votes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "vote", description = "All the vote methods"))
@SecurityScheme(securitySchemeName = "jwt", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "jwt")
public class VotesResource {
    
    @Inject
    VoteFacade voteFacade;

    @Inject
    @Claim("user_name")
    Optional<JsonString> userName;

    @POST
    @RolesAllowed({"EMPLOYEE"})    
    @SecurityRequirement(name = "jwt", scopes = {})    
    @APIResponses(value = {
        @APIResponse(responseCode = "401", description = "Unauthorized"),        
        @APIResponse(responseCode = "406", description = "Vote not acceptable", content = @Content(mediaType = "PLAIN TEXT")),
        @APIResponse(responseCode = "201", description = "Vote successfuly created.", content = @Content(mediaType = "PLAIN TEXT"))
    })
    @Operation(summary = "Register vote to current date")
    public Response register(@Valid VoteRequest voteRequest, @Context UriInfo info) {
        try {
            voteRequest.setCurrentUser(userName.get().getString());
            Vote vote = voteFacade.registerVote(voteRequest);
            Long voteId = vote.getId();
            URI uri = info.getAbsolutePathBuilder().path("/" + vote.getId()).build();    
            return Response.created(uri).entity(voteId).build(); 
            
        } catch(IllegalArgumentException | IllegalStateException iex){
            return Response.status(Response.Status.NOT_ACCEPTABLE).entity(iex.getMessage()).build();
        }
        
    }
    
    
    @GET
    @Path("/{areaTypeId}/between/{startDate}/{fromDate}")
    @RolesAllowed({"EMPLOYEE"})    
    @SecurityRequirement(name = "jwt", scopes = {})
    @APIResponses(value = {
        @APIResponse(responseCode = "401", description = "Unauthorized Error"),
        @APIResponse(responseCode = "200", description = "Return summary votes", content = @Content(schema = @Schema(implementation = SummaryVoteReport.class)))})
    @Operation(summary = "Return total votes by area type and dates")    
    public List<SummaryVoteReport> findByAreaTypeAndDates(@PathParam("areaTypeId") long areaTypeId, 
            @PathParam("startDate") String startDate,
            @PathParam("endDate") String endDate) {
        LocalDate startDateLD = DateTimeUtil.getLocalDate(startDate);
        LocalDate endDateLD = DateTimeUtil.getLocalDate(endDate);        
        return voteFacade.countVotesByAreaTypeAndDates(areaTypeId, startDateLD, endDateLD);
        
        
    }
    
    @GET
    @Path("/between/{startDate}/{fromDate}")
    @RolesAllowed({"EMPLOYEE"})    
    @SecurityRequirement(name = "jwt", scopes = {})
    @APIResponses(value = {
        @APIResponse(responseCode = "401", description = "Unauthorized Error"),
        @APIResponse(responseCode = "200", description = "Return summary votes", content = @Content(schema = @Schema(implementation = SummaryVoteReport.class)))})
    @Operation(summary = "Return total votes by dates")
    public List<SummaryVoteReport> findByDates(
            @PathParam("startDate") String startDate,
            @PathParam("endDate") String endDate) {
        LocalDate startDateLD = DateTimeUtil.getLocalDate(startDate);
        LocalDate endDateLD = DateTimeUtil.getLocalDate(endDate);        
        return voteFacade.countVotesByDates(startDateLD, endDateLD);
        
    }

}