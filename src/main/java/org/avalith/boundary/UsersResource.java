/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.avalith.boundary;

import java.util.Optional;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonString;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.avalith.vote.VO.Counter;
import org.avalith.vote.business.VoteFacade;
import org.avalith.vote.model.User;
import org.avalith.vote.VO.UserJWT;
import org.avalith.vote.util.TokenUtil;
import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.eclipse.microprofile.openapi.annotations.tags.Tags;

/**
 *
 * @author wduck
 */
@RequestScoped
@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tags(value = @Tag(name = "user", description = "All the user methods"))
@SecurityScheme(securitySchemeName = "jwt", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "jwt")
public class UsersResource {
    
    @Inject
    VoteFacade voteFacade;

    @Inject
    @Claim("user_name")
    Optional<JsonString> userName;

    @POST
    @PermitAll
    @Path("/token/{userName}/{pwd}")
    @APIResponses(value = {
        @APIResponse(responseCode = "400", description = "JWT generation error"),
        @APIResponse(responseCode = "200", description = "JWT successfuly created.", content = @Content(schema = @Schema(implementation = UserJWT.class)))})
    @Operation(summary = "Create JWT token by provided user name and pwd")
    public UserJWT getToken(@PathParam("userName") String userName, @PathParam("pwd") String pwd) throws Exception {
        return voteFacade.login(userName, pwd);
        
        
    }

    @GET
    @RolesAllowed({"ADMIN", "EMPLOYEE"})    
    @Path("/current")
    @SecurityRequirement(name = "jwt", scopes = {})
    @APIResponses(value = {
        @APIResponse(responseCode = "401", description = "Unauthorized Error"),
        @APIResponse(responseCode = "200", description = "Return user data", content = @Content(schema = @Schema(implementation = UserJWT.class)))})
    @Operation(summary = "Return user data by provided JWT token")
    public UserJWT getUser() {
        UserJWT user = new UserJWT();
        user.setName(userName.get().getString());
        return user;
    }

    @GET
    @RolesAllowed({"ADMIN"})    
    @Path("/{role}/total")
    @SecurityRequirement(name = "jwt", scopes = {})
    @APIResponses(value = {
        @APIResponse(responseCode = "401", description = "Unauthorized Error"),
        @APIResponse(responseCode = "200", description = "Return total users", content = @Content(schema = @Schema(implementation = Counter.class)))})
    @Operation(summary = "Return total users by role filte")
    public Counter totalUsers(@PathParam("role") String roleName) {
        Number total = voteFacade.totalUsersByRoleName(roleName);
        return new Counter(total);
       
        //return null;
    }
}