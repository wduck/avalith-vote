-- AreaType
INSERT INTO AreaType(id, name, description) VALUES (1, 'Team player', 'Team player');
INSERT INTO AreaType(id, name, description) VALUES (2, 'Technical Referent', 'Technical referent');
INSERT INTO AreaType(id, name, description) VALUES (3, 'Key Player', 'Key player'); 
INSERT INTO AreaType(id, name, description) VALUES (4, 'Client Satisfaction', 'Client satisfaction');  
INSERT INTO AreaType(id, name, description) VALUES (5, 'Motivation', 'Motivation');
INSERT INTO AreaType(id, name, description) VALUES (6, 'Fun', 'Fun');

-- Role
INSERT INTO Role(id, name, description) VALUES (1, 'ADMIN', 'admin');
INSERT INTO Role(id, name, description) VALUES (2, 'EMPLOYEE', 'Employee');

-- Person
INSERT INTO Person(id, firstName, lastName) VALUES (1, 'Wilman', 'Chamba Z.');
-- Person
INSERT INTO Person(id, firstName, lastName) VALUES (2, 'Florencia', 'Amores Diez'); 

-- User
-- pwd 12345
INSERT INTO User(id, name, password, person_id) VALUES (1, 'wduck@avalith.org', '827ccb0eea8a706c4c34a16891f84e7b', 1); 
INSERT INTO User(id, name, password, person_id) VALUES (2, 'florencia@avalith.org', '827ccb0eea8a706c4c34a16891f84e7b', 2); 

-- USER_ROLE
INSERT INTO user_role (user_id, role_id) VALUES (1,1);
INSERT INTO user_role (user_id, role_id) VALUES (1,2);

INSERT INTO user_role (user_id, role_id) VALUES (2,2);

