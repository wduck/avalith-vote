######################################
# vote project

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```
./mvnw quarkus:dev
```

## Packaging and running the application

The application can be packaged using `./mvnw package`.
It produces the `vote-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

The application is now runnable using `java -jar target/vote-1.0.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: `./mvnw package -Pnative`.

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: `./mvnw package -Pnative -Dquarkus.native.container-build=true`.

You can then execute your native executable with: `./target/vote-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/building-native-image-guide.

######################

Para compilar el proyecto y ejecutar en modo desarrollo realizar ejecutar la sentencias dentro del directorio del proyecto
./mvnw compile quarkus:dev

El presente proyecto esta desarrollado bajo Jakarta EE (JEE8) con Quarkus y aplica la arquitectura Multi-Capas, específicamente con 3 CAPAS:
CAPA VISTA --> Boundary API-REST
CAPA NEGOCIO --> business logica del negocio de la APP 
CAPA MODELO --> Modelo del negocio de la APP (Mapping OR)

Ademas aplica el Patron Facade dentro de la logica de negocio, asi como tambien en caso de convertirlo en microservicio a traves de su APi-REST 


Para visualizar el API luego de desplegar el proyecto es:
http://0.0.0.0:8080/swagger-ui

Home Page
http://0.0.0.0:8080/
http://localhost:8080/

- La autenticacion que se utiliza JWT.
Existe un archivo de inicializacion de la BD para cuando se despliega en modo DEV, dicho archivo se encuentra en: resources/data/import-dev.sql
Las credenciales para ingresar son con la clave 12345:
wduck@avalith.org  --> ROLES {ADMIN, EMPLOYEE}
florencia@avalith.org@avalith.org  --> ROLES {EMPLOYEE}

Además se adjunta un bosquejo del modelo del dominio para crear la app como imagen (disculpas está en borrador y a lápiz) con el nombre modelo-dominio.jpg






